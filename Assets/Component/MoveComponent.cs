﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

[GenerateAuthoringComponent]
public struct MoveComponent : IComponentData{
	public float speed;
}
