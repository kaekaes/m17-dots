﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct RotationComponent : IComponentData {
	public float angularSpeed;
}
