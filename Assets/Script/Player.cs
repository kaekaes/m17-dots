﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class Player : Disparable {

	public static Player instance;

	private Rigidbody2D rb;
	private float moveX, moveY;
	public float speed = 3f;

	public Disparo patron;

	public GameObject bulletPrefab;
	private Entity bulletEntity;
	private EntityManager entityManager;
	private bool canShoot = true;

	private float changeDirectionShoots, extraAngle, invertAngleShoots;

	void Start() {
		if (instance == null) instance = this;
		TryGetComponent<Rigidbody2D>(out rb);

		if (patron.changeDirection) changeDirectionShoots = patron.changeDirectionShootCount;
		if (patron.invertAngleXShoot) invertAngleShoots = patron.invertAngleXShootCount;

		entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
		var sett = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
		bulletEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletPrefab, sett);
	}

	void Update() {
		moveX = Input.GetAxis("Horizontal");
		moveY = Input.GetAxis("Vertical");

		if (Input.GetButton("Jump")) 
			Shoot();
		
	}

	void Shoot() {
		if (canShoot) {
			if (patron.changeDirection) {
				changeDirectionShoots--;
				if (changeDirectionShoots <= 0) {
					patron.rotateAfterTime *= -1;
					changeDirectionShoots = patron.changeDirectionShootCount;
				}
			}

			if (patron.invertAngleXShoot) {
				invertAngleShoots--;
				if (invertAngleShoots <= 0) {
					patron.angleXShoot *= -1;
					invertAngleShoots = patron.invertAngleXShootCount;
				}
			}

			switch (patron.shootType) {
				case ShootType.MULTISHOOT:
					MultiChot();
					break;
				case ShootType.ROUND:
					RoundChot();
					break;

			}
		}
	}

	private void RoundChot() {
		float angle = 360 / patron.quantity;
		for (int i = 0; i < patron.quantity; i++) {
			Entity bullet = entityManager.Instantiate(bulletEntity);

			Quaternion rot = Quaternion.Euler(new Vector3(0, 0, extraAngle));
			if (patron.quantity != 1) {
				rot = Quaternion.Euler(new Vector3(0, 0, (i * angle) + extraAngle));
			}
			AddComponentsToEntity(bullet, rot);

		}
		//print("instanciao");
		canShoot = false;
		if (patron.changeAngleXShoot)
			extraAngle = (extraAngle+patron.angleXShoot)%360;

		Invoke(nameof(canShootCC), patron.tts);

	}

	private void MultiChot() {
		float angle = 22.5f / ((patron.quantity - 1) / 2);

		//Aquí se podría hacer una native array para optimizar
		for (int i = 0; i < patron.quantity; i++) {
			Entity bullet = entityManager.Instantiate(bulletEntity);

			Quaternion rot = Quaternion.Euler(Vector3.zero);
			if (patron.quantity != 1) {
				rot = Quaternion.Euler(new Vector3(0, 0, -22.5f + (i * angle) + extraAngle));
			}
			AddComponentsToEntity(bullet, rot);
		}
		//print("instanciao");
		canShoot = false;
		if (patron.changeAngleXShoot)
			extraAngle = (extraAngle + patron.angleXShoot) % 360;

		Invoke(nameof(canShootCC), patron.tts);
	}

	private void AddComponentsToEntity(Entity bullet, Quaternion rot) {
		entityManager.SetComponentData(bullet, new Translation { Value = transform.position });
		entityManager.SetComponentData(bullet, new Rotation { Value = rot });
		entityManager.SetComponentData(bullet, new MoveComponent { speed = patron.speed });
		entityManager.SetComponentData(bullet, new RotationComponent { angularSpeed = patron.rotateAfterTime });
		entityManager.SetComponentData(bullet, new BulletComponent { playerBullet = true });
		if (patron.ttl != 0) {
			entityManager.SetComponentData(bullet, new TimeToLiveComponent { timeToLive = patron.ttl });
		}
	}

	private void ResetWeirdThings() {
		changeDirectionShoots = extraAngle = invertAngleShoots = 0;
	}

	private void canShootCC() {
		canShoot = true;
	}

	void FixedUpdate() {
		rb.velocity = new Vector2(moveX, moveY) * speed;
	}

}
