﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class Enemy : Disparable {

	private Rigidbody2D rb;
	private float moveX, moveY;

	private Disparo activePattern;
	public Disparo[] patrones;
	public bool shouldMove = true;
	public float speed;

	public GameObject bulletPrefab;
	private Entity bulletEntity;
	private EntityManager entityManager;
	private bool canShoot = true;

	private float changeDirectionShoots, extraAngle, invertAngleShoots;

	Vector3 startPos;
	float time;

	void Start() {
		TryGetComponent<Rigidbody2D>(out rb);
		entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
		var sett = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
		bulletEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletPrefab, sett);

		activePattern = patrones[0];

		startPos = transform.position;

		StartCoroutine(Disparar());
	}

	public void Update() {
		if(shouldMove)
			time += Time.deltaTime;
	}

	public float tiempoDeUsoPatron = 5, esperaEntrePatrones = 5;
	IEnumerator Disparar() {
		ResetWeirdThings();
		if (activePattern.shouldStop) {
			shouldMove = false;
			moveX = 0;
			moveY = 0;
		}

		for (float t = 0f; t < 1f; t += Time.deltaTime / tiempoDeUsoPatron) {
			Shoot();
			yield return null;
		}

		shouldMove = true;

		yield return new WaitForSeconds(esperaEntrePatrones);

		activePattern = patrones[Random.Range(0, patrones.Length)];

		StartCoroutine(Disparar());
	}
	private void ResetWeirdThings() {
		changeDirectionShoots = 0;
		extraAngle = 0;
		invertAngleShoots = 0;
	}
	void FixedUpdate() {
		if(shouldMove)
			rb.position = startPos + new Vector3(-Mathf.Abs(Mathf.Cos(time))/2, Mathf.Sin(time), 0) * speed;	
	}


	private void Shoot() {
		if (canShoot) {
			if (activePattern.changeDirection) {
				changeDirectionShoots--;
				if (changeDirectionShoots <= 0) {
					activePattern.rotateAfterTime *= -1;
					changeDirectionShoots = activePattern.changeDirectionShootCount;
				}
			}

			if (activePattern.invertAngleXShoot) {
				invertAngleShoots--;
				if (invertAngleShoots <= 0) {
					activePattern.angleXShoot *= -1;
					invertAngleShoots = activePattern.invertAngleXShootCount;
				}
			}

			switch (activePattern.shootType) {
				case ShootType.MULTISHOOT:
					MultiChot();
					break;
				case ShootType.ROUND:
					RoundChot();
					break;

			}
		}
	}

	private void MultiChot() {
		float angle = -22.5f / ((activePattern.quantity - 1) / 2);
		for (int i = 0; i < activePattern.quantity; i++) {
			Entity bullet = entityManager.Instantiate(bulletEntity);

			Quaternion rot = Quaternion.Euler(Vector3.zero);
			if (activePattern.quantity != 1) {
				rot = Quaternion.Euler(new Vector3(0, 0,180- (22.5f) + (i * angle) + extraAngle));
			}
			AddComponentsToEntity(bullet, rot, activePattern);
		}
		canShoot = false;
		if (activePattern.changeAngleXShoot)
			extraAngle = (extraAngle + activePattern.angleXShoot) % 360;

		Invoke(nameof(canShootCC), activePattern.tts);
	}

	private void RoundChot() {
		float angle = 360 / activePattern.quantity;
		for (int i = 0; i < activePattern.quantity; i++) {
			Entity bullet = entityManager.Instantiate(bulletEntity);

			Quaternion rot = Quaternion.Euler(new Vector3(0, 0, extraAngle));
			if (activePattern.quantity != 1) {
				rot = Quaternion.Euler(new Vector3(0, 0, (i * angle) + extraAngle));
			}
			AddComponentsToEntity(bullet, rot, activePattern);

		}
		//print("instanciao");
		canShoot = false;
		if (activePattern.changeAngleXShoot)
			extraAngle = (extraAngle + activePattern.angleXShoot) % 360;

		Invoke(nameof(canShootCC), activePattern.tts);

	}

	private void AddComponentsToEntity(Entity bullet, Quaternion rot, Disparo patron) {
		entityManager.SetComponentData(bullet, new Translation { Value = transform.position });
		entityManager.SetComponentData(bullet, new Rotation { Value = rot });
		entityManager.SetComponentData(bullet, new MoveComponent { speed = patron.speed });
		entityManager.SetComponentData(bullet, new RotationComponent { angularSpeed = patron.rotateAfterTime });
		entityManager.SetComponentData(bullet, new BulletComponent { playerBullet = false });
		if (patron.ttl != 0) {
			entityManager.SetComponentData(bullet, new TimeToLiveComponent { timeToLive = patron.ttl });
		}
	}

	private void canShootCC() {
		canShoot = true;
	}

}
